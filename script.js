function Log(message) {
    let cnl = document.getElementById("console");
    cnl.innerText += `${message}\n`;
}
function Clear() {
    document.getElementById("console").innerHTML = "";
}
function RandomNumber(min, max){
    let number = min - 0.5 + Math.random() * (max - min + 1);
    return number; 
}
class Warrior{
    constructor() {
        this.health = 100;   	
    }
    set Health(value){
        if(value>100){
            this.health = 100;
        }
        else if(value < 0){
            this.health = 0;
        }
        else{
            this.health = value;
        }
    }
    get Health(){
        return this.health;
    }
    Attack() {
        let damage = (RandomNumber(18, 25))*-1;
        Log(`Атака на ${Math.round(damage)*-1} урона`);
        return Math.round(damage);
    }
    HeavyAttack(){
        let damage = (RandomNumber(10, 35))*-1;
        Log(`Сильная Атака на ${Math.round(damage)*-1} урона`);
        return Math.round(damage);
      }
    Heal(){
        let heal = RandomNumber(10, 35);
        Log(`Ицеление на ${Math.round(heal)} здоровья`);
        return Math.round(heal);
    }
    ChooseAction(war){
        let number = Math.round(RandomNumber(1, 3));
        if(number == 1){
            return this.Attack();
        }
        if(number == 2){
            return this.HeavyAttack();
        }
        if(number == 3){
            return this.Heal();
        }
    }
}
class CompWarrior extends Warrior {
    ChooseAction (war) {
        if(this.health <=35){
            let number = Math.round(RandomNumber(1, 4));
            if(number == 1){
                return this.Attack();
            }
            if(number == 2){
                return this.HeavyAttack();
            }
            if(number == 3 || number == 4){
                Log("Выроятность излечения увеличена");
                return this.Heal();
            }     
        }
        else {
            return super.ChooseAction();
          }
    }
}
class Fight{
    constructor(warPlayer, warPc){
        this.warPlayer = warPlayer;
        this.warPc = warPc;
    }
    ChooseWarrior(){
        let number = Math.round(RandomNumber(1, 2));
        if(number == 1){
            Log(`Ваш ход`);
            return this.warPlayer;
        }
        else{
            Log(`Ходит компьютер`);
            return this.warPc;
        }
    }
    Do(){
        while(this.warPlayer.Health != 0 && this.warPc.Health !=0){
            let turn = this.ChooseWarrior();
            let impact = turn.ChooseAction();
            if (impact > 0) {
                turn.Health = turn.Health + impact;
                Log(`Ваше Здоровье = ${this.warPlayer.Health} Здоровье Противника = ${this.warPc.health}`);
            } else if (turn == this.warPlayer && impact < 0) {
                this.warPc.Health = this.warPc.Health + impact;
                Log(`Ваше Здоровье = ${this.warPlayer.Health} Здоровье Противника = ${this.warPc.health}`);
            } else if (turn == this.warPc && impact < 0) {
                this.warPlayer.Health = this.warPlayer.Health + impact;
                Log(`Ваше Здоровье = ${this.warPlayer.Health} Здоровье Противника = ${this.warPc.health}`);
            }
        }
        if(this.warPlayer.health == 0){
            return Log("Победил Компьютер");
        }
        else{
            return Log("Вы победили");
        } 
    }
}
function Start(){ 
    let player = new Warrior();
    let pc = new CompWarrior();
    let fight = new Fight(player, pc);
    fight.Do();
}
